extern crate ndarray;
extern crate rand;
extern crate three;

mod block;
mod neighborhood;
mod rule;
mod world;

use three::Object;

use block::Block;
use world::World;

fn main() {
    let world = World::noise((10, 10, 10), &mut rand::thread_rng());

    let mut window = three::Window::new("Hello 3D World!");
    window.scene.background = three::Background::Color(three::color::BLUE);

    let light = window.factory.point_light(three::color::WHITE, 1.0);
    light.set_position([5.0, 5.0, 12.0]);
    window.scene.add(light);

    let cube = three::Geometry::cuboid(1.0, 1.0, 1.0);
    let material = three::material::Lambert {
        color: three::color::GREEN,
        ..Default::default()
    };
    let cube_mesh = window.factory.mesh(cube, material);

    for (coords, block) in world.inner.indexed_iter() {
        if *block == Block::Air {
            continue;
        }

        let cube = window.factory.mesh_instance(&cube_mesh);
        cube.set_position([coords.0 as f32, coords.1 as f32, coords.2 as f32]);
        window.scene.add(cube);
    }

    let camera = window.factory.perspective_camera(75.0, 1.0..50.0);
    camera.set_position([12.0, 12.0, 12.0]);
    camera.look_at([12.0, 12.0, 12.0], [0.0, 0.0, 0.0], None);

    while window.update() {
        window.render(&camera);
    }
}
