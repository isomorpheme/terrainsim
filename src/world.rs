use ndarray::{Array, Array3, Shape};
use rand::{Rand, Rng};

use block::Block;

type Size = (usize, usize, usize);

#[derive(Debug)]
pub struct World {
    pub inner: Array3<Block>
}

impl World {
    pub fn new(size: Size) -> Self {
        World {
            inner: Array3::from_elem(size, Block::Air)
        }
    }

    pub fn noise<R: Rng>(size: Size, rng: &mut R) -> Self {
        let items = rng.gen_iter().take(size.0 * size.1 * size.2);
        World {
            inner: Array::from_iter(items).into_shape(size).expect("size is invalid")
        }
    }
}
