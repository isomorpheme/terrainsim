use rand::{Rand, Rng};
use rand::distributions::{Range, IndependentSample};

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Block {
    Air,
    Ground,
}

impl Rand for Block {
    fn rand<R: Rng>(rng: &mut R) -> Self {
        let variant = Range::new(0, 2).ind_sample(rng);
        match variant {
            0 => Block::Air,
            1 => Block::Ground,
            _ => unreachable!(),
        }
    }
}
